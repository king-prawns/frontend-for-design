// Import React
import React from 'react';

// Import Spectacle Core tags
import {
  Deck,
  Heading,
  Link,
  Slide,
} from 'spectacle';

// Import theme
import createTheme from 'spectacle/lib/themes/default';

// Require CSS
require('normalize.css');

const theme = createTheme(
  {
    primary: 'white',
    secondary: '#1F2022',
    tertiary: '#03A9FC',
    quaternary: '#CECECE',
  },
  {
    primary: 'Montserrat',
    secondary: 'Helvetica',
  }
);

export default class Presentation extends React.Component {
  async toggleVideo() {
    const video = document.querySelector('#video')
    try {
      if (video !== document.pictureInPictureElement)
        await video.requestPictureInPicture();
      else
        await document.exitPictureInPicture();

    } catch(error) {
      console.log(`> Argh! ${error}`);
    }
  }
  toggleAnimation () {
    const ball = document.querySelector("#ball_js")

    var options = {
      iterations: Infinity,
      iterationStart: 0,
      delay: 0,
      endDelay: 0,
      direction: 'alternate',
      duration: 1000,
      fill: 'forwards',
      easing: 'ease-out',
    }
    var keyframes = [
      { transform: 'translateY(-1000px) scaleY(2.5) scaleX(.2)', transformOrigin: '50% 0', filter: 'blur(40px)', opacity: 0 },
      { transform: 'translateY(0) scaleY(1) scaleX(1)',
      transformOrigin: '50% 50%',
      filter: 'blur(0)',
      opacity: 1 }
    ]

    let myAnimation = ball.animate(keyframes, options)
    
    setTimeout(() => {
      myAnimation.pause()
      setTimeout(() => {
        myAnimation.reverse()
      }, 3000)
    }, 3500)
  }
  startObserver () {
    /* global IntersectionObserver */
    const scroller = document.querySelector('#scroller');
    const sentinel = document.querySelector('#sentinel');
    const totalItem = document.querySelector('#totalItem');
    var counter = 1;

    function loadItems(n) {
      for (var i = 0; i < n; i++) {
        var newItem = document.createElement('div');
        newItem.classList.add('item');
        newItem.textContent = 'Item ' + counter++;
        scroller.appendChild(newItem);
      }
    }

    var intersectionObserver = new IntersectionObserver(entries => {
      // If intersectionRatio is 0, the sentinel is out of view
      // and we do not need to do anything.
      if (entries[0].intersectionRatio <= 0) {
        return;
      }
      loadItems(10);
      // appendChild will move the existing element, so there is no need to
      // remove it first.
      scroller.appendChild(sentinel);
      loadItems(5);
      totalItem.innerHTML = 'Loaded up to item ' + counter;
    });
    intersectionObserver.observe(sentinel);
  }
  startOnlineOffline () {
    var status = document.getElementById("status_online_offline");

    function updateOnlineStatus(event) {
      var condition = navigator.onLine ? "online" : "offline";

      status.className = condition;
      status.innerHTML = condition.toUpperCase();
    }

    window.addEventListener('online',  updateOnlineStatus);
    window.addEventListener('offline', updateOnlineStatus);
  }
  startPageLifecycle () {
    const status = document.querySelector('#status_lifecycle')
    document.addEventListener('visibilitychange', (event) => {
      console.warn('visibilitychange', document.visibilityState)
      var newItem = document.createElement('span');
      newItem.classList.add(document.visibilityState);
      newItem.textContent = document.visibilityState;
      status.appendChild(newItem);
    });
  }
  drawCanvas () {
    // settings
    var physics_accuracy  = 3,
      mouse_influence   = 20,
      mouse_cut         = 5,
      gravity           = 1200,
      cloth_height      = 30,
      cloth_width       = 50,
      start_y           = 20,
      spacing           = 7,
      tear_distance     = 60;

    var canvas,
      ctx,
      cloth,
      boundsx,
      boundsy,
      mouse = {
          down: false,
          button: 1,
          x: 0,
          y: 0,
          px: 0,
          py: 0
      };

    var Point = function (x, y) {
        this.x      = x;
        this.y      = y;
        this.px     = x;
        this.py     = y;
        this.vx     = 0;
        this.vy     = 0;
        this.pin_x  = null;
        this.pin_y  = null;
        
        this.constraints = [];
    };
    
    Point.prototype.update = function (delta) {
        if (mouse.down) {
            var diff_x = this.x - mouse.x,
                diff_y = this.y - mouse.y,
                dist = Math.sqrt(diff_x * diff_x + diff_y * diff_y);
    
            if (mouse.button === 1) {
                if (dist < mouse_influence) {
                    this.px = this.x - (mouse.x - mouse.px) * 1.8;
                    this.py = this.y - (mouse.y - mouse.py) * 1.8;
                }
              
            } else if (dist < mouse_cut) this.constraints = [];
        }

        this.add_force(0, gravity);

        delta *= delta;
        var nx = this.x + ((this.x - this.px) * .99) + ((this.vx / 2) * delta);
        var ny = this.y + ((this.y - this.py) * .99) + ((this.vy / 2) * delta);
    
        this.px = this.x;
        this.py = this.y;
    
        this.x = nx;
        this.y = ny;
    
        this.vy = this.vx = 0
    };

    Point.prototype.draw = function () {
        if (!this.constraints.length) return;

        var i = this.constraints.length;
        while (i--) this.constraints[i].draw();
    };

    Point.prototype.resolve_constraints = function () {
        if (this.pin_x != null && this.pin_y != null) {
            this.x = this.pin_x;
            this.y = this.pin_y;
            return;
        }
    
        var i = this.constraints.length;
        while (i--) this.constraints[i].resolve();
    
        this.x > boundsx ? this.x = 2 * boundsx - this.x : 1 > this.x && (this.x = 2 - this.x);
        this.y < 1 ? this.y = 2 - this.y : this.y > boundsy && (this.y = 2 * boundsy - this.y);
    };

    Point.prototype.attach = function (point) {
        this.constraints.push(
            new Constraint(this, point)
        );
    };

    Point.prototype.remove_constraint = function (constraint) {
        this.constraints.splice(this.constraints.indexOf(constraint), 1);
    };

    Point.prototype.add_force = function (x, y) {
        this.vx += x;
        this.vy += y;
      
        var round = 400;
        this.vx = ~~(this.vx * round) / round;
        this.vy = ~~(this.vy * round) / round;
    };

    Point.prototype.pin = function (pinx, piny) {
        this.pin_x = pinx;
        this.pin_y = piny;
    };

    var Constraint = function (p1, p2) {
        this.p1     = p1;
        this.p2     = p2;
        this.length = spacing;
    };

    Constraint.prototype.resolve = function () {
        var diff_x  = this.p1.x - this.p2.x,
            diff_y  = this.p1.y - this.p2.y,
            dist    = Math.sqrt(diff_x * diff_x + diff_y * diff_y),
            diff    = (this.length - dist) / dist;

        if (dist > tear_distance) this.p1.remove_constraint(this);

        var px = diff_x * diff * 0.5;
        var py = diff_y * diff * 0.5;

        this.p1.x += px;
        this.p1.y += py;
        this.p2.x -= px;
        this.p2.y -= py;
    };

    Constraint.prototype.draw = function () {
        ctx.moveTo(this.p1.x, this.p1.y);
        ctx.lineTo(this.p2.x, this.p2.y);
    };

    var Cloth = function () {
        this.points = [];

        var start_x = canvas.width / 2 - cloth_width * spacing / 2;

        for (var y = 0; y <= cloth_height; y++) {
            for (var x = 0; x <= cloth_width; x++) {
                var p = new Point(start_x + x * spacing, start_y + y * spacing);

                x !== 0 && p.attach(this.points[this.points.length - 1]);
                y === 0 && p.pin(p.x, p.y);
                y !== 0 && p.attach(this.points[x + (y - 1) * (cloth_width + 1)])

                this.points.push(p);
            }
        }
    };
    
    Cloth.prototype.update = function () {
        var i = physics_accuracy;

        while (i--) {
            var p = this.points.length;
            while (p--) this.points[p].resolve_constraints();
        }

        i = this.points.length;
        while (i--) this.points[i].update(.016);
    };

    Cloth.prototype.draw = function () {
        ctx.beginPath();

        var i = cloth.points.length;
        while (i--) cloth.points[i].draw();

        ctx.stroke();
    };

    function update() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        cloth.update();
        cloth.draw();

        requestAnimationFrame(update);
    }

    /* eslint-disable */
    function start() {
      canvas.onmousedown = function (e) {
      mouse.button  = e.which;
      mouse.px      = mouse.x;
      mouse.py      = mouse.y;
      var rect      = canvas.getBoundingClientRect();
      mouse.x       = e.clientX - rect.left,
      mouse.y       = e.clientY - rect.top,
      mouse.down    = true;
      e.preventDefault();
    };

    canvas.onmouseup = function (e) {
      mouse.down = false;
      e.preventDefault();
    };

    canvas.onmousemove = function (e) {
      mouse.px  = mouse.x;
      mouse.py  = mouse.y;
      var rect  = canvas.getBoundingClientRect();
      mouse.x   = e.clientX - rect.left,
      mouse.y   = e.clientY - rect.top,
      e.preventDefault();
    };
    /* eslint-enable */

    canvas.oncontextmenu = function (e) {
      e.preventDefault();
    };

    boundsx = canvas.width - 1;
    boundsy = canvas.height - 1;

    ctx.strokeStyle = '#888';

    cloth = new Cloth();

    update();
    }

    canvas = document.getElementById('canvas');
    ctx = canvas.getContext('2d');

    canvas.width  = 560;
    canvas.height = 350;

    start();
  }
  render() {
    return (
      <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={theme}
      >
        <Slide transition={['zoom']} bgColor="primary">
          <Heading size={1} fit lineHeight={1} textColor="tertiary"> 
            Frontend for Design
          </Heading>
        </Slide>
        <Slide transition={['fade']} bgColor="tertiary">
          <Heading size={3} lineHeight={1} textColor="secondary" style={{marginBottom: "60px"}}> 
            Picture in Picture (PIP)
          </Heading>
          <video id="video" controls src="https://storage.googleapis.com/media-session/caminandes/short.mp4"></video>
          <div className="button_wrapper">
            <span onClick={this.toggleVideo} className="button">Toggle</span>
          </div>
        </Slide>
        <Slide transition={['fade']} bgColor="secondary">
          <Heading size={3} lineHeight={1} textColor="primary" style={{marginBottom: "60px"}}> 
            Web Animation
          </Heading>
          <div className="ball_wrapper">
            <div id="ball_js" className="ball_js">JS</div>
            <div className="ball_css">CSS</div>
          </div>
          <div className="button_wrapper">
            <span onClick={this.toggleAnimation} className="button">Start</span>
          </div>
        </Slide>
        <Slide transition={['fade']} bgColor="primary">
          <Heading size={3} lineHeight={1} textColor="tertiary" style={{marginBottom: "60px"}}> 
            CSS Snap
          </Heading>
          <div className="slider">
            <section>
              <h1>Section One</h1>
            </section>
            <section>
              <h1>Section Two</h1>
            </section>
            <section>
              <h1>Section Three</h1>
            </section>
            <section>
              <h1>Section Four</h1>
            </section>
            <section>
              <h1>Section Five</h1>
            </section>
          </div>
        </Slide>
        <Slide transition={['fade']} bgColor="tertiary">
          <Heading size={3} lineHeight={1} textColor="secondary" style={{marginBottom: "60px"}}> 
            Intersection Observer
          </Heading>
          <div id="scroller">
            <div id="sentinel"></div>
          </div>
          <div id="totalItem" className="totalItem">Loaded up to item 0</div>
          <div className="button_wrapper">
            <span onClick={this.startObserver} className="button">Start</span>
          </div>
        </Slide>
        <Slide transition={['fade']} bgColor="secondary">
          <Heading size={3} lineHeight={1} textColor="primary" style={{marginBottom: "60px"}}> 
            Display Flexbox
          </Heading>
          <div>
            <div className="flexbox_wrapper">
              <div>1</div>
            </div>
            <div className="flexbox_wrapper">
              <div>1</div>
              <div>2</div>
            </div>
            <div className="flexbox_wrapper">
              <div>1</div>
              <div>2</div>
              <div>3</div>
            </div>
            <div className="flexbox_wrapper">
              <div>1</div>
              <div>2</div>
              <div>3</div>
              <div>4</div>
            </div>
            <div className="flexbox_wrapper">
              <div>1</div>
              <div>2</div>
              <div>3</div>
              <div>4</div>
              <div>5</div>
            </div>
            <div className="flexbox_wrapper">
              <div>1</div>
              <div>2</div>
              <div>3</div>
              <div>4</div>
              <div>5</div>
              <div>6</div>
            </div>
          </div>
        </Slide>
        <Slide transition={['fade']} bgColor="primary">
          <Heading size={3} lineHeight={1} textColor="tertiary" style={{marginBottom: "60px"}}> 
            Display Grid
          </Heading>
          <div className="grid_wrapper_wrapper">
            <div className="grid_wrapper">
              <div className="box a">A</div>
              <div className="box b">B</div>
              <div className="box c">C</div>
              <div className="box d">
                <div className="box e">E</div>
                <div className="box f">F</div>
                <div className="box g">G</div>
              </div>
            </div>
          </div>
        </Slide>
        <Slide transition={['fade']} bgColor="tertiary">
          <Heading size={3} lineHeight={1} textColor="secondary" style={{marginBottom: "60px"}}> 
            Canvas
          </Heading>
          <canvas id="canvas" width="560" height="350"></canvas>
          <div className="button_wrapper">
            <span onClick={this.drawCanvas} className="button">Draw</span>
          </div>
        </Slide>
        <Slide transition={['none']} bgColor="secondary">
          <Heading size={3} lineHeight={1} textColor="primary" style={{marginBottom: "60px"}}> 
            CSS is powerful
          </Heading>
          <div className="iframe_wrapper">
            <iframe src="http://diana-adrianne.com/purecss-zigario/" title="paint"></iframe>
          </div>
        </Slide>
        <Slide transition={['fade']} bgColor="primary">
          <Heading size={3} lineHeight={1} textColor="tertiary" style={{marginBottom: "60px"}}> 
            CSS Units
          </Heading>
          <div className="font_wrapper">
            <section className="font">
              <div className="font_title">
                0.5 EM
              </div>
              <div className="font_inner">
                <span className="font_inner_ref">50px</span>
                <div className="font_inner_em">
                  25px
                  <div className="font_inner_em">
                    12.5px 
                    <div className="font_inner_em">
                      6.25px
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section className="font">
              <div className="font_title">
                0.5 REM
              </div>
              <div className="font_inner">
                <span className="font_inner_ref">50px</span>
                <div className="font_inner_rem">
                  8px
                  <div className="font_inner_rem">
                    8px 
                    <div className="font_inner_rem">
                      8px
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section className="font">
              <div className="font_title">
                50 %
              </div>
              <div className="font_inner">
                <span className="font_inner_ref">50px</span>
                <div className="font_inner_perc">
                  25px
                  <div className="font_inner_perc">
                    12.5px
                    <div className="font_inner_perc">
                      6.25px
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section className="font">
              <div className="font_title">
                vw 2
              </div>
              <div className="font_inner">
                <span className="font_inner_ref">50px</span>
                <div className="font_inner_vw">
                  2% vw
                  <div className="font_inner_vw">
                    2% vw
                    <div className="font_inner_vw">
                      2% vw
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </Slide>
        <Slide transition={['fade']} bgColor="tertiary">
          <Heading size={3} lineHeight={1} textColor="secondary" style={{marginBottom: "60px"}}> 
            Online/Offline
          </Heading>
          <div className="online_offline">
            <span id="status_online_offline"></span>
          </div>
          <div className="button_wrapper">
            <span onClick={this.startOnlineOffline} className="button">Start</span>
          </div>
        </Slide>
        <Slide transition={['fade']} bgColor="secondary">
          <Heading size={3} lineHeight={1} textColor="primary" style={{marginBottom: "60px"}}> 
            Page Lifecycle API
          </Heading>
          <div className="lifecycle">
            <div id="status_lifecycle"></div>
          </div>
          <div className="button_wrapper">
            <span onClick={this.startPageLifecycle} className="button">Start</span>
          </div>
        </Slide>
        <Slide transition={['fade']} bgColor="primary">
          <Heading size={3} lineHeight={1} textColor="tertiary" style={{marginBottom: "60px"}}> 
            VisBug
          </Heading>
          <Link href="https://chrome.google.com/webstore/detail/visbug/cdockenadnadldjbbgcallicgledbeoc" target="_blank">
            <span className="link">External Link</span>
          </Link>
        </Slide>
        <Slide transition={['fade']} bgColor="tertiary">
          <Heading size={3} lineHeight={1} textColor="secondary" style={{marginBottom: "60px"}}> 
            Storybook
          </Heading>
          <Link href="http://react.carbondesignsystem.com/?selectedKind=Accordion&selectedStory=Default&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Fstories%2Fstories-panel" target="_blank">
            <span className="link_white">External Link</span>
          </Link>
        </Slide>
        <Slide transition={['fade']} bgColor="secondary">
          <Heading size={3} lineHeight={1} textColor="primary" style={{marginBottom: "60px"}}> 
            PWA
          </Heading>
          <Link href="https://pwa.sprinkle.space/" target="_blank">
            <span className="link_white">External Link</span>
          </Link>
        </Slide>
        <Slide transition={['zoom']} bgColor="primary">
          <Heading size={1} fit lineHeight={1} textColor="tertiary"> 
            Thank You
          </Heading>
        </Slide>
      </Deck>
    );
  }
}
